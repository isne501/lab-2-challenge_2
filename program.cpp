#include <iostream>
#include "list.h"
using namespace std;


void main()
{
	//Test integer list
	List<int> mynumlist;
	mynumlist.pushToHead(2);
	mynumlist.pushToHead(1);
	mynumlist.pushToTail(3);
	mynumlist.pushToTail(4);
	cout << "Integer lists : "; mynumlist.print();

	//Test character list
	cout << endl;
	List<char> mycharlist;
	mycharlist.pushToHead('S');
	mycharlist.pushToTail('A');
	mycharlist.pushToTail('M');
	mycharlist.pushToTail('E');
	cout << "Character lists : "; mycharlist.print();

	//Test float list
	cout << endl;
	List<float> myfloatlist;
	myfloatlist.pushToTail(3.99);
	myfloatlist.pushToTail(3.98); 
	myfloatlist.pushToTail(3.97); 
	cout << "Float lists : ";  myfloatlist.print();
	
	//Test pophead function
	cout << endl;
	cout << "\npopHead in character lists : " << mycharlist.popHead() << endl;
	cout << "Character lists now : "; mycharlist.print();
	
	//Test poptail function
	cout << endl;
	cout << "\npopTail in integer lists : " << mynumlist.popTail() << endl;
	cout << "Integer  lists now : "; mynumlist.print();
	cout << endl;

	//Test search function
	cout << endl;
	cout << "Searhing : 3.98 in Float lists" << endl;
	if (myfloatlist.search(3.98) == true) //If there is.
		cout << "The 3.98 is in the list.";
	else //If there isn't.
		cout << "The 3.98 isn't in the list.";

	cout << endl;
	system("PAUSE");
	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?

}