#ifndef LIST
#define LIST

template <class T>
class Node {
public:
	T data;
	Node<T> *next, *prev;
	Node<T>()
	{
		next = prev = 0;
	}
	Node<T>(T el, Node *n = 0, Node *p = 0)
	{
		data = el; next = n; prev = p;
	}
};

template <class T>
class List {
public:
	List() { head = tail = 0; }

	int isEmpty() { return head == 0; }

	~List()
	{
		for (Node<T> *p; !isEmpty(); ) 
		{
			p = head->next;
			delete head;
			head = p;
		}
	}

	void pushToHead(T el)
	{
		head = new Node<T>(el, head, 0);
		if (tail == 0)
		{
			tail = head;
		}
		else
		{
			head->next->prev = head;
		}
	}

	//Push to tail function
	void pushToTail(T el)
	{
		Node<T> *tmp = new Node<T>(el, 0, tail); //Create pointer that contain element and point to NULL ,making previous point to old tail
		if (tail == 0) //if there is nothing in list
		{
			head = tail = tmp; 
		}
		else //if there are something in list
		{
			tail->next = tmp;  //making tail link to new tail
			tail = tmp; //move old tail to new tail
		}
	}

	//delete head function
	T popHead()
	{
		T el = head->data; //store data before delete
		Node<T> *tmp = head; //make pointer point to head
		if (head == tail) //if there is only one list
		{
			head = tail = 0; //make head and tail point to null
		}
		else
		{
			head = head->next; //move old head to new head 
			head->prev = 0; //make previous head to null
		}
		delete tmp; //delete old head
		return el; //return element
	}

	//delete tail function
	T popTail()
	{
		T el = tail->data; //Store tail data before delete tail
		Node<T> *nowNode = head; //Create node point to head
		Node<T> *atTail = tail; //Create node point to tail
		if (head == tail)  //If head and tail point at same locate
		{
			head = tail = 0; //Make head and tail point to NULL
			delete nowNode; //DELETE LIST
		}
		else //If head and tail point at not same locate
		{
			tail = tail->prev; //make tail point to previous tail
			tail->next = NULL; //make tail link to NULL
			delete atTail; //Delete old tail
			return el; //Return element that old tail stored
		}
	}

	//Check element
	bool search(T el)
	{
		Node<T> *check = head; //Make node point to head
		while (check->next != NULL) //Prevent while loop out of scope
		{
			if (check->data == el) //if element same as data that list store return true to booleen
			{
				return true;
			}
			else //if element not same as data that list store make check pointer moving to other list
			{
				check = check->next;
			}
		}
		return false; //if there is not same as element return false
	}

	//Show list
	void print()
	{
		if (head == tail)
		{
			cout << head->data;
		}
		else
		{
			Node<T> *tmp = head;
			while (tmp != tail)
			{
				cout << tmp->data;
				tmp = tmp->next;
			}
			cout << tmp->data;
		}
	}
private:
	Node<T> * head, *tail;
};

#endif